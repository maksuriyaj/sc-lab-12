package count;

/**
 * Lab 12
 * 
 * Main class that count words.
 * 
 * @author Usa Sammapun
 *
 */
public class WordCounterMain {
	public static void main(String[] args) {
		WordCounter counter = new WordCounter("poem.txt");
		counter.count();
		System.out.println(counter.getCountData());		
	}
}
