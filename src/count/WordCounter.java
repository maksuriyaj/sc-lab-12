package count;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class WordCounter {

	private String filename;
	private HashMap<String,Integer> wordCount;
	
	public WordCounter(String filename) {
		this.filename = filename;
		this.wordCount = new HashMap<String,Integer>();
	}
	
	public void count() {
		try {
			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);

			String line;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				countOneLine(line);
			}	
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}		
		catch (IOException e){
			System.err.println("Error reading from file");
		}		

	}
	
	/**
	 * Read files and increment counts for each word found
	 */
	private void countOneLine(String message) {
		String[] data = message.split(" ");

		for (String word : data) {
			String w = word.trim();

			int count = 1;
			if (wordCount.containsKey(w))
				count = 1 + wordCount.get(w);
			wordCount.put(w, count);
		}
	}	
	
	public int hasWord(String word) {
		if (wordCount.containsKey(word))
			return wordCount.get(word);
		return 0;
	}
	public String getCountData() {
		String data = "";
		for (String word : wordCount.keySet())
			data += word + "=" + wordCount.get(word) + "\n";
		return data;
	}

}
