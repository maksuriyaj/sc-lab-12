package phonebook;

public class PhoneBookMain {
	public static void main(String[] args) {
		PhoneBook book = new PhoneBook();
		book.readContact("phonebook.txt");
		book.displayReport();
	}
}
