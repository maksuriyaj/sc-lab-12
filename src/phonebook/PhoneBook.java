package phonebook;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class PhoneBook {

	private ArrayList<Contact> contactList = new ArrayList<Contact>();
	
	public void readContact(String filename) {
		try {
			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);

			String line;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] data = line.split(",");
				String name = data[0].trim();
				String phone = data[1].trim();
				contactList.add(new Contact(name, phone));
			}	
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}		
		catch (IOException e){
			System.err.println("Error reading from file");
		}		
	}
	
	public void displayReport() {
		System.out.println("--------- Java Phone Book ---------");
		System.out.println("Name\tPhone");
		System.out.println("====\t=====");
		for (Contact contact : contactList) {
			System.out.println(contact.getName() + "\t" + contact.getNumber());
		}
	}
}
